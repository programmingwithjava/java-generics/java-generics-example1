/*
* Generics allows us to create classes, interfaces and methods that take types as
* parameters that are called type parameters.
*
* argument passed for a type parameter can either be a class or an interface.
* interfaces themselves can also specify type parameters.
*
* here we are using a single bound but java allows using multiple bounds.
* rules still apply. you can only extend from a single class but implement multiple interfaces.
* if you are specifying multiple bounds, the class must come first.
* example: <T extends Player & Coach & Manager> -> here Player would be a class, Coach & Manager would be interfaces. 
* */
public class Main {
    public static void main(String[] args) {
        FootballPlayer serdar = new FootballPlayer("serdar");
        SoccerPlayer mert = new SoccerPlayer("mert");
        SoccerPlayer osman = new SoccerPlayer("osman");
        BaseballPlayer onur = new BaseballPlayer("onur");

        Team<SoccerPlayer> fenerbahce = new Team<>("fenerbahce");
        fenerbahce.addPlayer(osman);
        
        Team<SoccerPlayer> galatasaray = new Team<>("galatasaray");
        galatasaray.addPlayer(mert);

        System.out.println(fenerbahce.numPlayers());

        Team<BaseballPlayer> baseballTeam = new Team<>("baseballTeam");
        baseballTeam.addPlayer(onur);

        Team<FootballPlayer> footballTeam = new Team<>("footballTeam");
        footballTeam.addPlayer(serdar);
        
        /*
         * we can't run the below line since we are using a type parameter for the matchResult method too.
         * Which is the right thing to do. 
         * */
        //fenerbahce.matchResult(baseballTeam, 3, 0);
        
        fenerbahce.matchResult(galatasaray, 3, 0);
        
        System.out.println("\nRankings:");
        System.out.println(fenerbahce.getName() + ": " + fenerbahce.ranking());
        System.out.println(galatasaray.getName() + ": " + galatasaray.ranking());
        
        System.out.println(fenerbahce.compareTo(galatasaray));
    }
}
