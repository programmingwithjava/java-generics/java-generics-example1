import java.util.ArrayList;

/*
 * here we are using a "bounded type parameter". T is a bounded type parameter here.
 * type parameter we will accept for this class will be any type that extends from Player or a subclass of player.
 * we are implementing the Comparable interface so that we can compare the teams and sort them in the rankings output of the main class.
 * it doesn't make sence to compare football teams against baseball teams etc. 
 * so we are using Team<T> type parameter for the Comparable interface. it is important to understand this part.
* */
public class Team<T extends Player> implements Comparable<Team<T>>{
    private String name;
    private int played = 0;
    private int won = 0;
    private int lost = 0;
    private int tie = 0;

    private ArrayList<T> members = new ArrayList<>();

    public Team(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public boolean addPlayer(T player){
        if(members.contains(player)){
            System.out.println(player.getName() + " is already in the team.");
            return false;
        }
        members.add(player);
        System.out.println(player.getName() + " was added to the team " + this.name);
        return true;
    }

    public int numPlayers(){
        return this.members.size();
    }

    /*
     * we need to use type parameter in matchResult method also otherwise we could be able to 
     * have match results between different types of teams like between a soccer team and a baseball 
     * team like so : fenerbahce.matchResult(baseballTeam, 3, 0);
     * */
    public void matchResult(Team<T> opponent, int ourScore, int theirScore){
    	String message;
        if(ourScore > theirScore){
            won++;
            message = " won against ";
        }
        else if(ourScore < theirScore){
            lost++;
            message = " lost against ";
        }
        else{
            tie++;
            message = " drew with ";
        }
        played++;
        if (opponent != null){
        	System.out.println(this.getName() + message + opponent.getName());
            opponent.matchResult(null, theirScore, ourScore);
        }
    }

    public int ranking(){
        return won * 3 + tie;
    }

    /*
     * if we have a set of objects that implement Comparable we can sort 
     * their list using the static "sort" method of the Collections class.
     * this compareTo method below will be used for that static sort method.
     * in other words, the objects in that list will be sorted using this compareTo method we just overrided. 
     * */
	@Override
	public int compareTo(Team<T> team) {
		/*
		 * if this team has more points that the team provided in the argument, then this team 
		 * should come before the team in the argument. so return -1.
		 * */
		if(this.ranking() > team.ranking()) {
			return -1;
		}else if(this.ranking() < team.ranking()) {
			return 1;
		}else {
			return 0;
		}

	}
}
